#!/bin/sh

set -x

RCPATH=$(dirname $(perl -MCwd -e 'print Cwd::abs_path shift' "$0"))
CURDATETIME=$(date +%Y%m%d%H%M%S)

OLD_STYLE_EMACS_INIT=${HOME}/.emacs
OLD_STYLE_EMACS_D=${HOME}/.emacs.d

if [ -f ${OLD_STYLE_EMACS_INIT} ]; then
  mv ${OLD_STYLE_EMACS_INIT} ${OLD_STYLE_EMACS_INIT}.bak.${CURDATETIME}
fi

if [ ! -L ${OLD_STYLE_EMACS_D} -a -d ${OLD_STYLE_EMACS_D} ]; then
  mv ${OLD_STYLE_EMACS_D} ${OLD_STYLE_EMACS_D}.bak.${CURDATETIME}
fi

ln -sf ${RCPATH} ${HOME}/.emacs.d
