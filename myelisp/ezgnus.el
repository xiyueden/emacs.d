;;; ezgnus.el --- Making using Gnus easy  -*- lexical-binding: t; -*-

;; Copyright (C) 2023 Xiyue Deng <manphiz@gmail.com>

;; Author: Xiyue Deng <manphiz@gmail.com>
;; Version: 0.1
;; Created: 21 Aug 2023
;; Package-Requires: (cl-lib gnus smtpmail subr-x)

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Comments:

;; This add-on provides a few helper functions to make setting up Gnus easier.
;; As an example, suppose a user named "Jane Doe" has an email
;; "jane.doe@example.com", we may have the following simple settings:

;; (setq ezgnus-default-email "jane.doe@example.com")
;; (setq ezgnus-default-user-name "Jane Doe")
;; (setq ezgnus-default-send-server "smtp.example.com")

;; (ezgnus-email-setting
;;  :email "jane.doe@example.com"
;;  :fetch-server-address "imap.example.com"
;;  :fetch-protocol "imaps"
;;  :send-server-address "smtp.example.com"
;;  :send-protocol "smtp"
;;  :inbox-folder "Inbox"
;;  :trash-folder "Trash"
;;  :name "Jane Doe"
;;  :signature "Best,\nJane Doe")

;; These should give you a reasonable group based Gnus setting so that you can
;; just continue to enjoy the power of Gnus.

;; A few more convenience function can make setting up a Gmail or Outlook
;; account even easier.  For example,

;; (ezgnus-gmail-setting :email "jane.doe@gmail.com" :name "Jane Doe")

;; or

;; (ezgnus-outlook-setting :email "jane.doe@outlook.com" :name "Jane Doe")

;; For news, one can use the following to subscribe to some topics on a server:

;; (ezgnus-news-setting :server "news.gmane.io"
;;                      :topic-list '("gmane.announce"
;;                                    "gmane.linux.debian.user.announce"))

;; Currently it supports `imap' and `smtp' for email.  Other protocol support
;; may be implemented, and happily accepting contributions!

;;; Code:

(require 'cl-lib)
(require 'gnus)
(require 'smtpmail)
(require 'subr-x)

(defgroup ezgnus nil
  "A customization group for Ezgnus.")

(defcustom ezgnus-default-email ""
  "Default email."
  :type '(string)
  :group 'ezgnus)

(defcustom ezgnus-default-user-name ""
  "Default user name."
  :type '(string)
  :group 'ezgnus)

(defcustom ezgnus-default-send-server ""
  "Default send mail server."
  :type '(string)
  :group 'ezgnus)

(defcustom ezgnus-default-signature nil
  "Default signature for default email."
  :type 'string
  :group 'ezgnus)

(defcustom ezgnus-enable-split-view nil
  "Enable split view in Gnus group list."
  :type '(boolean)
  :group 'ezgnus)

(defcustom ezgnus-show-extended-user-agents t
  "Enable extended information of user-agent fields to see the MUA of an email."
  :type '(boolean)
  :group 'ezgnus)

(defun ezgnus--add-visible-headers (header-fields)
  "Add headers in HEADER-FIELDS to gnus-visible-headers if not present yet."
  (dolist (header-field header-fields)
    (if (not (string-match header-field gnus-visible-headers))
        (setq gnus-visible-headers
              (concat gnus-visible-headers "\\|^" header-field ":")))))

(defun ezgnus--process-topic-and-record (account-regexp topic)
  "Recursively process TOPIC and record in the ACCOUNT-TOPIC-MAP."
  (cond
   ((stringp topic)
    (let (account)
      (if (string-match-p account-regexp topic)
          (setq account (replace-regexp-in-string account-regexp "\\1" topic))
        ;; Put unmatched topics in "Misc".
        (setq account "Misc"))
      (if (not (string= topic "Gnus"))
          (let ((topic-list (gethash account ezgnus--account-topic-map ())))
            (if (not (member topic topic-list))
                (add-to-list 'topic-list topic t))
            (puthash account topic-list ezgnus--account-topic-map)))))
   ((listp topic)
    (dolist (current-topic topic)
      (ezgnus--process-topic-and-record account-regexp current-topic)))
   (t
    (error "Unknown topic type: %s" topic))))

(defun ezgnus--reconstruct-gnus-topic-alist ()
  "Reconstruct gnus-topic-alist to be grouped by account/topic.

This should be called in gnus-started-hook so that
gnus-topic-alist has already been popped by existing topics."
  (when gnus-topic-alist
    (setq ezgnus--account-topic-map (make-hash-table :test 'equal)
          ezgnus--topic-alist ())

    (let ((topic-alist gnus-topic-alist)
          (account-regexp
           "^nn[^+:]+\\+\\([^+:]+\\(?:@\\|\\.\\)[^+:]+\\)\\:[^+:]+$"))
      (dolist (topic topic-alist)
        (ezgnus--process-topic-and-record account-regexp topic)))

    (dolist (topic-topology ezgnus--topic-topology)
      (when (listp topic-topology)
        (let* ((account (car (flatten-tree topic-topology)))
               (topics (gethash account ezgnus--account-topic-map nil)))
          (when topics
            (let ((sorted-topics (sort topics 'string<))
                  (should-append (not (string= account "Gnus"))))
              (add-to-list 'ezgnus--topic-alist
                           (flatten-tree `(,account ,sorted-topics))
                           should-append)
              (remhash account ezgnus--account-topic-map))))))

    (maphash (lambda (account topics)
               (let ((sorted-topics (sort topics 'string<)))
                 (add-to-list 'ezgnus--topic-alist
                              (flatten-tree `(,account ,sorted-topics)) t)))
             ezgnus--account-topic-map)

    (setq gnus-topic-alist ezgnus--topic-alist)))

(defun ezgnus--record-news-topic-to-process (topic)
  "Record the given TOPIC-LIST to subscribe."
  (puthash topic topic ezgnus--news-topics-to-subscribe))

(defun ezgnus--process-news-topics ()
  "Subscribe to the topic in the process list if not already subscribed."

  (dolist (topic (flatten-tree gnus-topic-alist))
    (if (gethash topic ezgnus--news-topics-to-subscribe)
        (remhash topic ezgnus--news-topics-to-subscribe)
      (if (string-prefix-p "nntp" topic)
          (gnus-group-toggle-subscription topic))))

  (maphash (lambda (topic ignored)
             (gnus-subscribe-group topic))
           ezgnus--news-topics-to-subscribe))

(defun ezgnus--topic-callback ()
  "Set proper gnus topics.

Register proper gnus-topic-topology and gnus-topic-alist through
ezgnus-topic-registration."
  ;; Set up top level Gnus topic.
  (add-to-list 'ezgnus--topic-topology '(("Misc" visible nil nil)))
  (add-to-list 'ezgnus--topic-topology '("Gnus" visible))

  (setq gnus-topic-topology ezgnus--topic-topology)
  (if (not (null ezgnus-default-signature))
      (add-to-list 'ezgnus--posting-styles
                   '(".*" (signature ezgnus-default-signature)) t))
  (setq gnus-posting-styles ezgnus--posting-styles)

  (ezgnus--process-news-topics)

  (advice-add 'gnus-group-list-groups :before
              #'(lambda (&optional ign-arg1 ign-arg2 ign-arg3 ign-arg4)
                  (ezgnus--reconstruct-gnus-topic-alist)))
  (gnus-group-list-groups))

(defun ezgnus--startup-callback ()
  "Settings to set after Gnus startup."
  (setq user-full-name ezgnus-default-user-name
        user-mail-address ezgnus-default-email)

  (when ezgnus-show-extended-user-agents
    (ezgnus--add-visible-headers '("User-Agent" "X-Newsreader" "X-Mailer")))

  (add-hook 'gnus-group-mode-hook #'gnus-topic-mode)

  (with-eval-after-load "mm-encode"
    (add-to-list 'mm-discouraged-alternatives "text/html")
    (add-to-list 'mm-discouraged-alternatives "text/richtext"))

  (gnus-demon-init)
  (setq gnus-demon-timestep 300)
  ;; Check for new mail every 1 timestep
  (gnus-demon-add-handler 'gnus-demon-scan-news 1 t)

  ;; Don't crash gnus if disconnected
  (defadvice gnus-demon-scan-news (around gnus-demon-timeout
                                          activate)
    "Timeout for Gnus."
    (with-timeout
        (30 (message "Gnus timed out."))
      ad-do-it))

  (when ezgnus-enable-split-view
    (gnus-add-configuration
     '(article
       (horizontal 1.0
                   (vertical .25 (group 1.0))
                   (vertical 1.0
                             (summary 0.25 point)
                             (article 1.0)))))

    (gnus-add-configuration
     '(summary
       (horizontal 1.0
                   (vertical .25 (group 1.0))
                   (vertical 1.0 (summary 1.0 point))))))

  (add-hook 'gnus-started-hook #'ezgnus--topic-callback))

(defun ezgnus--register-email-secondary-select-method
    (email fetch-protocol fetch-server-address authenticator inbox-folder
           trash-folder &optional fetch-port)
  "Register an email-based secondary select method."
  (ezgnus--assert-not-nil email)
  (cond
   ((string= fetch-protocol "imap")
    (ezgnus--assert-not-nil fetch-server-address)
    (ezgnus--assert-not-nil inbox-folder)
    (ezgnus--assert-not-nil trash-folder)
    (when (null fetch-port)
      (setq fetch-port (ezgnus--get-default-port-of-supported-protocol
                        (ezgnus--get-supported-fetch-protocols-to-ports)
                        fetch-protocol)))
    (let ((folder-prefix (ezgnus--get-folder-prefix email fetch-protocol))
          (inbox (ezgnus--get-prefixed-folder email fetch-protocol
                                              inbox-folder))
          (trash (ezgnus--get-prefixed-folder email fetch-protocol
                                              trash-folder)))
      (add-to-list 'gnus-secondary-select-methods
                   `(nnimap ,email
                            (nnimap-inbox ,inbox)
                            (nnimap-address ,fetch-server-address)
                            (nnimap-server-port ,fetch-port)
                            (nnimap-user ,email)
                            (nnimap-stream ssl)
                            (nnimap-authenticator ,authenticator)
                            (nnmail-expiry-target ,trash)
                            (nnmail-expiry-wait immediate))
                   t)))
   (t
    (error "Email protocol `%s' is not supported yet." fetch-protocol))))

(defun ezgnus--register-news-secondary-select-method
    (account &optional topic-list)
  "Register a news-based secondary select method."
  (ezgnus--assert-not-nil account)
  (add-to-list 'gnus-secondary-select-methods `(nntp ,account) t)
  (if topic-list
      (dolist (topic topic-list)
        (let ((prefixed-topic (ezgnus--get-prefixed-folder account "nntp"
                                                           topic)))
          (ezgnus--record-news-topic-to-process prefixed-topic)))))

(defun ezgnus--register-account-in-topic-topology (account)
  "Register an ACCOUNT in the gnus-topic-topology."
  (add-to-list 'ezgnus--topic-topology `((,account visible nil nil)) t))

(defun ezgnus--register-posting-style
    (account send-protocol send-server-address send-port name signature
             signature-file)
  "Register a posting style.

ACCOUNT, SEND-PROTOCOL, and SEND-SERVER-ADDRESS are required.  If
both SIGNATURE and SIGNATURE-FILE are specified, the latter takes
precedence."
  (ezgnus--assert-not-nil account)
  (ezgnus--assert-not-nil send-protocol)
  (ezgnus--assert-not-nil send-server-address)

  (when (null send-port)
    (setq send-port (ezgnus--get-default-port-of-supported-protocol
                     (ezgnus--get-supported-send-protocols-to-ports)
                     send-protocol)))

  (let* ((ezgnus--posting-style-pattern
          (ezgnus--get-posting-style-pattern account))
         (ezgnus--x-message-smtp-method
          (string-join
           `(,send-protocol ,send-server-address ,send-port ,account) " "))
         (ezgnus--posting-style
          `(,ezgnus--posting-style-pattern
            (address ,account))))

    (if name
        (add-to-list 'ezgnus--posting-style `(name ,name) t))

    (if signature-file
        (add-to-list 'ezgnus--posting-style
                     `(signature-file ,signature-file) t)
      (if signature
          (add-to-list 'ezgnus--posting-style `(signature ,signature) t)))

    (add-to-list 'ezgnus--posting-style
                 `("X-Message-SMTP-Method" ,ezgnus--x-message-smtp-method)
                 t)

    (add-to-list 'message-server-alist
                 `(,account . ,ezgnus--x-message-smtp-method)
                 t)
    (add-to-list 'ezgnus--posting-styles ezgnus--posting-style t)))

(defun ezgnus--get-posting-style-pattern (account)
  "Generate a gnus-posting-style regexp given an ACCOUNT."
  (concat ".*" (replace-regexp-in-string "\\." "\\\\." account) ".*"))

(defun ezgnus--get-folder-prefix (account protocol)
  "Generate a folder prefix given an ACCOUNT and its PROTOCOL."
  (cond
   ((string= protocol "imap")
    (let ((protocol-prefix "nnimap"))
      (concat protocol-prefix "+" account ":")))
   ((string= protocol "nntp")
    (let ((protocol-prefix "nntp"))
      (concat protocol-prefix "+" account ":")))
   (t
    (error "Protocol `%s' not supported." protocol))))

(defun ezgnus--get-prefixed-folder (account protocol folder)
  "Generate a prefixed folder given an ACCOUNT and its PROTOCOL."
  (let ((ezgnus-folder-prefix (ezgnus--get-folder-prefix account protocol)))
    (concat ezgnus-folder-prefix folder)))

(defun ezgnus--init ()
  "Set default settings and register hook.

Note: this needs to run before everything else otherwise Gnus may
report no default server error, and some values are being re-set
in the gnus-startup-hook.  Ideally we should figure out a way to
do this without user intervention."
  (setq gnus-always-read-dribble-file t
        gnus-summary-line-format
        "%U%R%z|%16&user-date;| %2B%(%[%4L:%* %-23,23f%]%) %s\n"
        gnus-thread-sort-functions '(gnus-thread-sort-by-date)
        gnus-fetch-old-headers 'some
        gnus-buttonized-mime-types '("multipart/signed")
        gnus-message-replysign t
        gnus-select-method '(nnml "")
        gnus-use-cache t

        smtpmail-default-smtp-server ezgnus-default-send-server
        send-mail-function 'smtpmail-send-it
        message-send-mail-function 'smtpmail-send-it
        message-sendmail-f-is-evil nil
        message-sendmail-envelope-from 'header

        gnus-secondary-select-methods ()
        ezgnus--topic-topology ()
        ezgnus--posting-styles ()
        ezgnus--news-topics-to-subscribe (make-hash-table :test 'equal))

  (add-hook 'gnus-startup-hook #'ezgnus--startup-callback)

  (gnus-delay-initialize))

(defmacro ezgnus--assert-not-nil (var &optional message)
  (list 'if `(null ,var)
        (list 'if `(null ,message)
              (list 'error "Should set `%s'" (symbol-name var))
              `(list 'error ,message))))

(defun ezgnus--get-supported-fetch-protocols-to-ports ()
  "Returns a hash table of supported fetch protocols to ports.

Note that due to how the value is typically used it should be a
string."
  (let ((supported-fetch-protocols-to-ports
         (make-hash-table :test 'equal)))
    (puthash "imap" "imaps" supported-fetch-protocols-to-ports)

    supported-fetch-protocols-to-ports))

(defun ezgnus--get-supported-send-protocols-to-ports ()
  "Returns a hash table of supported send protocols to ports.

Note that due to how the value is typically used it should be a
string."
  (let ((supported-send-protocols-to-ports
         (make-hash-table :test 'equal)))
    (puthash "smtp" "587" supported-send-protocols-to-ports)

    supported-send-protocols-to-ports))

(defun ezgnus--get-default-port-of-supported-protocol
    (supported-protocols-to-ports protocol)
  "Get the corresponding port of a supported protocol.

If a given protocol is not in the supported list it will end up
in an error."
  (let ((port (gethash protocol supported-protocols-to-ports nil)))
    (ezgnus--assert-not-nil port
                            (format "Protocol `%s' is not supported." protocol))
    port))

;;;###autoload
(cl-defun ezgnus-email-setting (&key email fetch-server-address
                                     fetch-protocol send-server-address
                                     send-protocol inbox-folder trash-folder
                                     folder-list (authenticator nil)
                                     (fetch-port nil) (send-port nil) (name nil)
                                     (signature nil) (signature-file nil))
  "Convenient function to set general settings of an email account.

EMAIL, FETCH-SERVER-ADDRESS, FETCH-PROTOCOL, SEND-SERVER-ADDRESS,
SEND-PROTOCOL, TRASH-FOLDER, and FOLDER-LIST are required.
FETCH-PORT and SEND-PORT will choose a reasonable default based
on their corresponding protocols and can be overridden for
customization.  If both SIGNATURE and SIGNATURE-FILE are set,
`signature-file' takes precedence."
  (ezgnus--assert-not-nil email)
  (ezgnus--assert-not-nil fetch-server-address)
  (ezgnus--assert-not-nil fetch-protocol)
  (ezgnus--assert-not-nil send-server-address)
  (ezgnus--assert-not-nil send-protocol)
  (ezgnus--assert-not-nil trash-folder)

  (ezgnus--register-email-secondary-select-method
   email fetch-protocol fetch-server-address authenticator inbox-folder
   trash-folder fetch-port)

  (ezgnus--register-posting-style email send-protocol send-server-address
                                  send-port name signature signature-file)

  (ezgnus--register-account-in-topic-topology email))

;;;###autoload
(cl-defun ezgnus-gmail-setting (&key email (name nil) (authenticator nil)
                                     (signature nil) (signature-file nil))
  "Convenient function to set up a Gmail account."
  (ezgnus--assert-not-nil email)

  (ezgnus-email-setting
   :email email
   :fetch-server-address "imap.gmail.com"
   :fetch-protocol "imap"
   :send-server-address "smtp.gmail.com"
   :send-protocol "smtp"
   :inbox-folder "INBOX"
   :trash-folder "[Gmail]/Trash"
   :authenticator authenticator
   :name name
   :signature signature
   :signature-file signature-file))

;;;###autoload
(cl-defun ezgnus-outlook-setting (&key email (name nil) (authenticator nil)
                                       (signature nil) (signature-file nil))
  "Convenient function to set up an Outlook account."
  (ezgnus--assert-not-nil email)

  (ezgnus-email-setting
   :email email
   :fetch-server-address "outlook.office365.com"
   :fetch-protocol "imap"
   :send-server-address "smtp.office365.com"
   :send-protocol "smtp"
   :inbox-folder "Inbox"
   :trash-folder "Deleted"
   :authenticator authenticator
   :name name
   :signature signature
   :signature-file signature-file))

;;;###autoload
(cl-defun ezgnus-news-setting (&key server (topic-list nil))
  "Convenient function to set up a news server."
  (ezgnus--assert-not-nil server)

  (let ((protocol "nntp"))
    (ezgnus--register-news-secondary-select-method server topic-list)
    (ezgnus--register-account-in-topic-topology server)))

;; Establish default settings.
(ezgnus--init)

(provide 'ezgnus)

;;; ezgnus.el ends here
