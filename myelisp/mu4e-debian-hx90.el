;;; mu4e-site.el --- custom mu4e -*-coding: utf-8-*-

;;; Commentary:
;;;   All mu4e settings for custom mail accounts.  Depending on
;;;   offlineimap for IMAP access, and msmtp for SMTP access.
;;; Code:

(require 'use-package)

(use-package mu4e
  :defer t
  :custom
  (mu4e-maildir "~/Maildir")
  (mu4e-get-mail-command "true")
  (mu4e-update-interval 300)
  (mu4e-index-cleanup t)
  (mu4e-index-lazy-check nil)
  (mu4e-view-show-images t)
  (mu4e-view-show-addresses t)
  (mu4e-use-fancy-chars t)
  (shr-color-visible-luminance-min 80)
  (mail-user-agent 'mu4e-user-agent)
  (message-kill-buffer-on-exit t)
  (send-mail-function 'smtpmail-send-it)
  (mu4e-sent-messages-behavior 'delete)

  :bind
  (:map mu4e-view-mode-map
        ("<DEL>" . scroll-down))

  :config
  (add-to-list 'mu4e-bookmarks
               '( :name "Flagged/Starred"
                  :key ?f
                  :query "flag:flagged AND NOT flag:trashed")
               t)
  (add-to-list 'mu4e-bookmarks
               '( :name "Drafts"
                  :key ?d
                  :query "flag:draft AND NOT flag:trashed")
               t)

  (add-to-list 'mu4e-header-info-custom
               '( :useragent .
                  ( :name "User-Agent"
                    :shortname "User-Agent"
                    :help "The User-Agent set by the mail client of sender"
                    :function
                    (lambda (msg)
                      (or (mu4e-fetch-field msg "User-Agent") "")))))
  (add-to-list 'mu4e-header-info-custom
               '( :x-mailer .
                  ( :name "X-Mailer"
                    :shortname "X-Mailer"
                    :help "The X-Mailer set by the mail client of sender"
                    :function
                    (lambda (msg)
                      (or (mu4e-fetch-field msg "X-Mailer") "")))))
  (add-to-list 'mu4e-header-info-custom
               '( :x-newsreader .
                  ( :name "X-Newsreader"
                    :shortname "X-Newsreader"
                    :help "The X-Newsreader set by the mail client of sender"
                    :function
                    (lambda (msg)
                      (or (mu4e-fetch-field msg "X-Newsreader") "")))))
  (add-to-list 'mu4e-view-fields :useragent t)
  (add-to-list 'mu4e-view-fields :x-mailer t)
  (add-to-list 'mu4e-view-fields :x-newsreader t)
  (setq mu4e-headers-fields
        '((:human-date . 12)
          (:maildir . 20)
          (:flags . 6)
          (:mailing-list . 10)
          (:from . 22)
          (:subject)))

  (with-eval-after-load "mm-decode"
    (add-to-list 'mm-discouraged-alternatives "text/html")
    (add-to-list 'mm-discouraged-alternatives "text/richtext"))

  (setq mu4e-contexts
        `(,(make-mu4e-context
            :name "1-manphiz@gmail.com"
            :enter-func (lambda()
                          (mu4e-message "Entering manphiz@gmail.com context")
                          (setq message-default-mail-headers
                                (concat "X-Message-SMTP-Method: "
                                        "smtp smtp.gmail.com 587 "
                                        "manphiz@gmail.com")))
            :match-func (lambda (msg)
                          (when msg
                            (string-match-p
                             "/manphiz@gmail.com"
                             (mu4e-message-field msg :maildir))))
            :vars '((user-mail-address . "manphiz@gmail.com")
                    (user-full-name . "Xiyue Deng")
                    (mu4e-sent-folder . "/manphiz@gmail.com/[Gmail].Sent Mail")
                    (mu4e-drafts-folder . "/manphiz@gmail.com/[Gmail].Drafts")
                    (mu4e-trash-folder . "/manphiz@gmail.com/[Gmail].Trash")
                    (mu4e-compose-signature . "Xiyue Deng")))

          ,(make-mu4e-context
            :name "2-dengxiyue@gmail.com"
            :enter-func (lambda()
                          (mu4e-message "Entering dengxiyue@gmail.com context")
                          (setq message-default-mail-headers
                                (concat "X-Message-SMTP-Method: "
                                        "smtp smtp.gmail.com 587 "
                                        "dengxiyue@gmail.com")))
            :match-func (lambda (msg)
                          (when msg
                            (string-match-p
                             "/dengxiyue@gmail.com"
                             (mu4e-message-field msg :maildir))))
            :vars '((user-mail-address . "dengxiyue@gmail.com")
                    (user-full-name . "Xiyue Deng")
                    (mu4e-sent-folder . "/dengxiyue@gmail.com/[Gmail].Sent Mail")
                    (mu4e-drafts-folder . "/dengxiyue@gmail.com/[Gmail].Drafts")
                    (mu4e-trash-folder . "/dengxiyue@gmail.com/[Gmail].Trash")
                    (mu4e-compose-signature . "Xiyue Deng")))

          ,(make-mu4e-context
            :name "3-xiyueden@usc.edu"
            :enter-func (lambda()
                          (mu4e-message "Entering xiyueden@usc.edu context")
                          (setq message-default-mail-headers
                                (concat "X-Message-SMTP-Method: "
                                        "smtp smtp.gmail.com 587 "
                                        "xiyueden@usc.edu")))
            :match-func (lambda (msg)
                          (when msg
                            (string-match-p
                             "/xiyueden@usc.edu"
                             (mu4e-message-field msg :maildir))))
            :vars '((user-mail-address . "xiyueden@usc.edu")
                    (user-full-name . "Xiyue Deng")
                    (mu4e-sent-folder . "/xiyueden@usc.edu/[Gmail].Sent Mail")
                    (mu4e-drafts-folder . "/xiyueden@usc.edu/[Gmail].Drafts")
                    (mu4e-trash-folder . "/xiyueden@usc.edu/[Gmail].Trash")
                    (mu4e-compose-signature
                     . (concat "Xiyue Deng, Ph.D.\n"
                               "Information Sciences Institute\n"
                               "University of Southern California\n"))))

          ,(make-mu4e-context
            :name "4-xiyuedeng@gmail.com"
            :enter-func (lambda()
                          (mu4e-message "Entering xiyuedeng@gmail.com context")
                          (setq message-default-mail-headers
                                (concat "X-Message-SMTP-Method: "
                                        "smtp smtp.gmail.com 587 "
                                        "xiyuedeng@gmail.com")))
            :match-func (lambda (msg)
                          (when msg
                            (string-match-p
                             "/xiyuedeng@gmail.com"
                             (mu4e-message-field msg :maildir))))
            :vars '((user-mail-address . "xiyuedeng@gmail.com")
                    (user-full-name . "Xiyue Deng")
                    (mu4e-sent-folder . "/xiyuedeng@gmail.com/[Gmail].Sent Mail")
                    (mu4e-drafts-folder . "/xiyuedeng@gmail.com/[Gmail].Drafts")
                    (mu4e-trash-folder . "/xiyuedeng@gmail.com/[Gmail].Trash")
                    (mu4e-compose-signature . "Xiyue Deng")))

          ,(make-mu4e-context
            :name "5-xiyueden@gmail.com"
            :enter-func (lambda()
                          (mu4e-message "Entering xiyueden@gmail.com context")
                          (setq message-default-mail-headers
                                (concat "X-Message-SMTP-Method: "
                                        "smtp smtp.gmail.com 587 "
                                        "xiyueden@gmail.com")))
            :match-func (lambda (msg)
                          (when msg
                            (string-match-p
                             "/xiyueden@gmail.com"
                             (mu4e-message-field msg :maildir))))
            :vars '((user-mail-address . "xiyueden@gmail.com")
                    (user-full-name . "Xiyue Deng")
                    (mu4e-sent-folder . "/xiyueden@gmail.com/[Gmail].Sent Mail")
                    (mu4e-drafts-folder . "/xiyueden@gmail.com/[Gmail].Drafts")
                    (mu4e-trash-folder . "/xiyueden@gmail.com/[Gmail].Trash")
                    (mu4e-compose-signature . "Xiyue Deng")))

          ,(make-mu4e-context
            :name "6-phurphy@gmail.com"
            :enter-func (lambda()
                          (mu4e-message "Entering phurphy@gmail.com context")
                          (setq message-default-mail-headers
                                (concat "X-Message-SMTP-Method: "
                                        "smtp smtp.gmail.com 587 "
                                        "phurphy@gmail.com")))
            :match-func (lambda (msg)
                          (when msg
                            (string-match-p
                             "/phurphy@gmail.com"
                             (mu4e-message-field msg :maildir))))
            :vars '((user-mail-address . "phurphy@gmail.com")
                    (user-full-name . "Phurphy")
                    (mu4e-sent-folder . "/phurphy@gmail.com/[Gmail].Sent Mail")
                    (mu4e-drafts-folder . "/phurphy@gmail.com/[Gmail].Drafts")
                    (mu4e-trash-folder . "/phurphy@gmail.com/[Gmail].Trash")
                    (mu4e-compose-signature . "Phurphy")))

          ,(make-mu4e-context
            :name "7-bamphiz@gmail.com"
            :enter-func (lambda()
                          (mu4e-message "Entering bamphiz@gmail.com context")
                          (setq message-default-mail-headers
                                (concat "X-Message-SMTP-Method: "
                                        "smtp smtp.gmail.com 587 "
                                        "bamphiz@gmail.com")))
            :match-func (lambda (msg)
                          (when msg
                            (string-match-p
                             "/bamphiz@gmail.com"
                             (mu4e-message-field msg :maildir))))
            :vars '((user-mail-address . "bamphiz@gmail.com")
                    (user-full-name . "Bamphiz")
                    (mu4e-sent-folder . "/bamphiz@gmail.com/[Gmail].Sent Mail")
                    (mu4e-drafts-folder . "/bamphiz@gmail.com/[Gmail].Drafts")
                    (mu4e-trash-folder . "/bamphiz@gmail.com/[Gmail].Trash")
                    (mu4e-compose-signature . "Bamphiz")))

          ,(make-mu4e-context
            :name "8-henrylaioffer@gmail.com"
            :enter-func (lambda()
                          (mu4e-message "Entering henrylaioffer@gmail.com context")
                          (setq message-default-mail-headers
                                (concat "X-Message-SMTP-Method: "
                                        "smtp smtp.gmail.com 587 "
                                        "henrylaioffer@gmail.com")))
            :match-func (lambda (msg)
                          (when msg
                            (string-match-p
                             "/henrylaioffer@gmail.com"
                             (mu4e-message-field msg :maildir))))
            :vars '((user-mail-address . "henrylaioffer@gmail.com")
                    (user-full-name . "Henry Laioffer")
                    (mu4e-sent-folder . "/henrylaioffer@gmail.com/[Gmail].Sent Mail")
                    (mu4e-drafts-folder . "/henrylaioffer@gmail.com/[Gmail].Drafts")
                    (mu4e-trash-folder . "/henrylaioffer@gmail.com/[Gmail].Trash")
                    (mu4e-compose-signature . "Henry Laioffer")))
          )))

(provide 'mu4e-debian-hx90)

;;; mu4e-site.el ends here
