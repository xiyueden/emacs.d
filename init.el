;;; init.el --- Emacs initialization -*- coding: utf-8; lexical-binding: t -*-

;;; Commentary:
;; General Emacs settings.

;;; Code:

;; Global settings

(setq inhibit-startup-screen t
      column-number-mode t
      global-hi-lock-mode t
      load-prefer-newer t
      default-major-mode 'text-mode
      truncate-lines nil
      truncate-partial-width-windows nil
      auto-revert-remote-files t
      plstore-encrypt-to "manphiz@gmail.com")
(transient-mark-mode t)
(show-paren-mode t)
(menu-bar-mode -1)
(global-auto-revert-mode t)

(setq custom-file (concat user-emacs-directory "custom.el"))
(when (file-exists-p custom-file)
  (load custom-file))

;; Programming related.
(setq c-default-style '((c-mode . "k&r")
                        (c++-mode . "stroustrup")
                        (java-mode . "java"))
      c-offsets-alist '((inline-open . 0)
                        (arglist-intro . +)
                        (innamespace . 0))
      gdb-many-windows t
      compilation-scroll-output 'first-error
      ruby-insert-encoding-magic-comment nil)
(setq-default indent-tabs-mode nil)
(add-hook 'prog-mode-hook #'(lambda () (setq fill-column 80)))

;; Alpha settings for emacs
(set-frame-parameter (selected-frame) 'alpha '(100 80))
(add-to-list 'default-frame-alist '(alpha 100 80))

;; Enable subword mode for prog-mode
(add-hook 'prog-mode-hook #'subword-mode)

;; Spell check settings
(with-eval-after-load "ispell"
  (let (ispell-available)
    (cond
     ((executable-find "hunspell")
      (setq ispell-program-name "hunspell")
      (setq ispell-local-dictionary "en_US")
      (setq ispell-available t)
      (when (string-equal system-type "gnu/linux")
        (setq ispell-local-dictionary-alist
              '(("en_US"
                 "[[:alpha:]]"
                 "[^[:alpha:]]"
                 "[']" t ("-d" "en_US") nil utf-8))
              ispell-dictionary "en_US")))
     ((executable-find "aspell")
      (setq ispell-program-name "aspell")
      (setq ispell-extra-args '("--sug-mode=ultra" "--lang=en_US"))
      (setq ispell-available t)))
    (when ispell-available
      (add-hook 'text-mode-hook #'turn-on-flyspell)
      (add-hook 'message-mode-hook #'turn-on-flyspell)
      ;; Flyspell in 26.1 is buggy when used with eglot. Enable on 26.2beta+
      (when (not (and (version< emacs-version "26.1.50")
                      (version<= "26.0.50" emacs-version)))
        (add-hook 'prog-mode-hook #'flyspell-prog-mode)))))

(setq auth-sources '("~/.authinfo.json.gpg" "~/.authinfo.gpg"))

(defun my-early-exit ()
  "Helper function to exit init.el early"
  (with-current-buffer " *load*"
    (goto-char (point-max))))

;; Set up for Emacs <25.
;; load profile base on Emacs version
(when (version< emacs-version "25.0.50")
  ;; Load local zenburn theme for older versions.
  (add-to-list 'custom-theme-load-path (concat user-emacs-directory "themes"))
  (load-theme 'zenburn t)
  (my-early-exit))

;; TLS 1.3 implementation in Emacs <26.3 is broken. Disabling.
(when (version< emacs-version "26.2.50")
  (setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3"))

;; Add local config files.
(add-to-list 'load-path (concat user-emacs-directory "myelisp"))

;; Enable line number when available
(if (version<= "26.0.50" emacs-version)
    (global-display-line-numbers-mode t))

;; Set up repository for Emacs 25+
(require 'package)

(defun my-elpa-archive (path)
  "Use https only if gnutls is available or else fallback to HTTP."
  (let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                      (not (gnutls-available-p))))
         (proto (if no-ssl "http://" "https://")))
    (concat proto path)))

(defun my-set-up-package-archives (package-archive-info-list)
  "Update package archive info with a given info list.

Each item contains 3 info: (archive-name archive-url
archive-priority).  The url should avoid the protocol part, which
will be auto-added depending on the availability of gnutls."
  (setq package-archives nil
        package-archive-priorities nil)
  (dolist (current-info package-archive-info-list nil)
    (if (not (equal (length current-info) 3))
        (error (format "Package archive incorrect, should be \
(name url priority).  Current value: (%s)"
                       (mapconcat 'prin1-to-string current-info " "))))
    (let ((info-name (nth 0 current-info))
          (info-url (nth 1 current-info))
          (info-priority (nth 2 current-info)))
      (add-to-list 'package-archives
                   (cons info-name (my-elpa-archive info-url)) t)
      (add-to-list 'package-archive-priorities
                   `(,info-name . ,info-priority)))))

(my-set-up-package-archives '(("gnu" "elpa.gnu.org/packages/" 100)
                              ("nongnu" "elpa.nongnu.org/nongnu/" 80)
                              ("melpa-stable" "stable.melpa.org/packages/" 50)
                              ("melpa" "melpa.org/packages/" 10)))

(when (version< emacs-version "27.0")
  (package-initialize))

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)

;; Package configurations.

;; Built-in packages.
(use-package bug-reference-mode
  :defer t
  :hook ((prog-mode . bug-reference-prog-mode)
         (gnus-mode . bug-reference-mode)
         (erc-mode . bug-reference-mode)
         (rcirc-mode . bug-reference-mode)))

(use-package conf-unix
  :defer t
  :hook ((conf-unix-mode . display-line-numbers-mode)
         (conf-unix-mode . flyspell-prog-mode)))

(use-package epa
  :defer t
  :custom
  (epa-pinentry-mode 'loopback))

(use-package erc
  :defer t
  :preface
  (defun my-normalize-server (server)
    "Normalize server address to retain only 2nd level domain names."
    (replace-regexp-in-string ".*\\.\\([^.]+\\.[^.]+\\)" "\\1" server))

  (defun my-password-for-machine-login (auth-source machine login)
    "Query password from a given auth-source in netrc format."
    (let ((query-result (car (auth-source-netrc-parse :file auth-source
                                                      :host machine
                                                      :user login
                                                      :max 1))))
      (alist-get "password" query-result nil nil #'equal)))

  (defun my-nickserv-identify-command (server password)
    "Customize nickserv identify command."
    (cond
     ;; Add custom logic for exotic servers, like
     ;; (string-match-p "exotic\\.server\\.com" server)
     (t
      (concat "NickServ IDENTIFY " password))))

  (defun my-erc-after-connect-hook (server nick)
    "Send the nickserv identify command after connected to a server."
    (let* ((server (my-normalize-server server))
           (command (my-nickserv-identify-command
                     server
                     (my-password-for-machine-login
                      (cl-find-if #'file-exists-p auth-sources)
                      server nick))))
      (erc-message "PRIVMSG" command)))

  (defun my-erc-connect ()
    (interactive)
    (erc-tls :server "irc.oftc.net"
             :port 6697
             :nick "manphiz"))

  :init
  (add-hook 'erc-after-connect #'my-erc-after-connect-hook)

  :custom
  (erc-enable-logging 'erc-log-all-but-server-buffers)
  (erc-log-channels-directory (concat user-emacs-directory "erclogs/"))
  (erc-save-buffer-on-part nil)
  (erc-save-queries-on-quit nil)
  (erc-log-write-after-insert t)
  (erc-log-write-after-send t)
  (erc-log-file-coding-system 'utf-8)
  (erc-timestamp-only-if-changed-flag nil)
  (erc-prompt-for-password nil)
  (erc-prompt-for-nickserv-password nil)
  (erc-autojoin-channels-alist
   '(("oftc.net"
      "#debian-emacs" "#debian-devel" "#debian-mentors" "#debian-qa")))

  :config
  (add-to-list 'erc-modules 'notifications)
  (add-to-list 'erc-modules 'spelling)
  (add-to-list 'erc-modules 'log)
  (add-to-list 'erc-modules 'stamp)
  (erc-update-modules))

(use-package flymake)

(use-package generic-x
  :defer t)

(use-package gnus
  :defer t
  :custom
  (gnus-init-file (concat user-emacs-directory "gnus.el")))

(use-package icomplete
  :defer t
  :custom
  (fido-vertical-mode t)
  (completion-styles '(partial-completion substring flex)))

(use-package latex
  :defer t
  :custom
  (TeX-auto-save t)
  (TeX-parse-self t)
  (TeX-master nil)
  :hook (((LaTeX-mode bibtex-mode) . display-line-numbers-mode)
         ((LaTeX-mode bibtex-mode) . flyspell-mode)))

(use-package cperl-mode
  :config
  (add-to-list 'interpreter-mode-alist '("perl" . cperl-mode)))

(use-package rcirc
  :defer t
  :preface
  (defadvice rcirc (before rcirc-read-from-authinfo activate)
    "Allow rcirc to read authinfo via the auth-source API.
Adapted from https://www.emacswiki.org/emacs/rcircAutoAuthentication"
    (unless arg
      (dolist (p (auth-source-search :port '("nickserv" "bitlbee" "quakenet")
                                     :require '(:host :port :user :secret)
                                     :max 100))
        (let ((secret (plist-get p :secret))
              (method (intern (plist-get p :port))))
          (add-to-list 'rcirc-authinfo
                       (list (plist-get p :host)
                             method
                             (plist-get p :user)
                             (if (functionp secret)
                                 (funcall secret)
                               secret)))))))

  :custom
  (rcirc-reconnect-delay 120)
  (rcirc-reconnect-attempts 1000)
  (rcirc-log-flag t)
  (rcirc-log-directory (concat user-emacs-directory "rcirclog"))
  (rcirc-log-process-buffers t)
  (rcirc-time-format "%Y-%m-%d %H:%M ")
  (rcirc-omit-responses '("JOIN" "PART" "QUIT" "NICK" "AWAY"))

  :config
  (rcirc-track-minor-mode 1)
  (setq rcirc-server-alist
        '(("irc.oftc.net"
           :channels
           ("#debian-devel" "#debian-emacs" "#debian-mentors" "#debian-newmaint"
            "#debian-qa" "#salsaci" "#debci")
           :port 6697
           :nick "manphiz"
           :encryption tls)
          ("irc.libera.chat"
           :channels ("#emacs")
           :port 6697
           :nick "manphiz"
           :encryption tls)))

  :hook ((rcirc-mode . (lambda ()
                         (flyspell-mode 1)
                         (set (make-local-variable 'scroll-conservatively)
                              8192)))))

(use-package whitespace
  :custom
  (whitespace-style '(face trailing tab-mark lines-tail))
  (whitespace-line-column 80)
  :hook ((prog-mode LaTeX-mode) . whitespace-mode)
  :config
  (set-face-attribute 'whitespace-line nil
                      :foreground 'unspecified
                      :underline t))

;; Local packages
(use-package auth-source-xoauth2-plugin
  :after oauth2
  :init
  (setq auth-source-debug 'trivia)
  :config
  (auth-source-xoauth2-plugin-mode t))

(use-package bazel-mode
  :defer t)

(use-package matlab-mode
  :defer t)

;; ELPA packages.
(use-package ace-window
  :ensure t
  :defer t
  :bind
  (("M-o" . ace-window)))

(use-package activities
  :ensure t
  :defer t
  :init
  (activities-mode)
  (activities-tabs-mode)
  ;; Prevent `edebug' default bindings from interfering.
  (setq edebug-inhibit-emacs-lisp-mode-bindings t)

  :bind
  (("C-x C-a C-n" . activities-new)
   ;; As resuming is expected to be one of the most commonly used
   ;; commands, this binding is one of the easiest to press.
   ("C-x C-a C-a" . activities-resume)
   ("C-x C-a C-s" . activities-suspend)
   ("C-x C-a C-k" . activities-kill)
   ;; This binding mirrors, e.g. "C-x t RET".
   ("C-x C-a RET" . activities-switch)
   ("C-x C-a g" . activities-revert)
   ("C-x C-a l" . activities-list)))

(use-package apache-mode
  :ensure t
  :defer t)

(use-package auctex
  :ensure t
  :defer t
  :after latex)

(use-package bind-key)

(use-package boxquote
  :ensure t
  :defer t)

(use-package magit
  :ensure t
  :defer t
  :custom (magit-view-git-manual-method 'woman))

(use-package bison-mode
  :ensure t
  :defer t
  :mode ("\\.lpp\\'" "\\.ypp\\'"))

(use-package clojure-mode
  :ensure t
  :defer t)

(use-package cmake-mode
  :ensure t
  :defer t)

(use-package company
  :ensure t
  :defer t)

(use-package corfu
  :ensure t
  :defer t
  ;; :init
  ;; (global-corfu-mode)
  ;; (corfu-echo-mode)
  ;; (corfu-popupinfo-mode)
  ;; :custom
  ;; (corfu-cycle t)
  ;; (corfu-auto t)
  )

(use-package corfu-terminal
  :ensure t
  :defer t
  :after corfu
  ;; :init
  ;; (unless (display-graphic-p)
  ;;   (corfu-terminal-mode +1))
  )

(use-package csv-mode
  :ensure t
  :defer t)

(use-package dart-mode
  :ensure t
  :defer t
  :custom
  (dart-format-on-save t))

(use-package debian-el
  :ensure t
  :defer t)

(use-package dpkg-dev-el
  :ensure t
  :defer t)

(use-package debpaste
  :ensure t
  :defer t)

(use-package dockerfile-mode
  :ensure t
  :defer t)

(when (version< emacs-version "29")
  (use-package docker-tramp
    :ensure t
    :defer t))

(use-package eglot
  :ensure t
  :defer t
  :if (and (version<= "26.2.50" emacs-version)
           (not (eq system-type 'windows-nt)))
  :after yasnippet
  :hook
  (prog-mode . eglot-ensure)
  ((debian-control-mode
    debian-changelog-mode
    debian-copyright-mode
    debian-autopkgtest-control-mode) . eglot-ensure)
  :config
  (let ((debian-modes '(debian-control-mode
                        debian-changelog-mode
                        debian-copyright-mode
                        debian-autopkgtest-control-mode)))
    (dolist (debian-mode debian-modes)
      (add-to-list 'eglot-server-programs
                   `(,debian-mode "debputy" "lsp" "server"
                                  "--ignore-language-ids")))))

(use-package elfeed
  :ensure t
  :defer t
  :custom
  (url-queue-timeout 30)
  (elfeed-feeds
   '("https://planet.emacslife.com/atom.xml"
     "https://planet.debian.org/atom.xml"
     "https://planetpython.org/rss20.xml"
     "https://perl.theplanetarium.org/atom.xml"
     "https://www.planetgolang.dev/index.xml"
     "http://blog.golang.org/feed.atom"
     "https://blog.rust-lang.org/feed.xml")))

(use-package exec-path-from-shell
  :if (or (memq window-system '(mac ns x))
          (daemonp))
  :ensure t
  :config
  (dolist (var '("SSH_AUTH_SOCK" "SSH_AGENT_PID" "GPG_AGENT_INFO" "LANG"
                 "LC_CTYPE" "NIX_SSL_CERT_FILE" "NIX_PATH" "DEBEMAIL"
                 "DEBFULLNAME" "TERM"))
    (add-to-list 'exec-path-from-shell-variables var))
  (exec-path-from-shell-initialize))

(use-package format-all
  :ensure t
  :defer t)

(use-package git-modes
  :ensure t
  :defer t)

(use-package gnuplot
  :ensure t
  :defer t
  :mode ("\\.gpi\\'" "\\.gnu\\'"))

(use-package go-mode
  :ensure t
  :defer t)

(use-package graphviz-dot-mode
  :ensure t
  :defer t)

(use-package keychain-environment
  :ensure t
  :config
  (keychain-refresh-environment))

(use-package meson-mode
  :ensure t
  :defer t)

(use-package muttrc-mode
  :ensure nil
  :defer t
  :mode (("\\muttrc\\'" . muttrc-mode)
         ("/mutt" . message-mode))
  :hook (message-mode . turn-on-auto-fill))

(use-package nginx-mode
  :ensure t
  :defer t)

(use-package oauth2
  :ensure t
  :init
  (setq oauth2-debug t))

(use-package ol-notmuch
  :ensure t
  :defer t)

(use-package paredit
  :ensure t
  :defer t)

(use-package po-mode
  :ensure t
  :defer t)

(use-package py-isort
  :ensure t
  :defer t)

(use-package pyvenv
  :ensure t
  :defer t)

(use-package rust-mode
  :ensure t
  :defer t
  :hook (rust-mode . cargo-minor-mode))

(use-package scala-mode
  :ensure t
  :defer t)

(use-package treemacs
  :ensure t
  :defer t
  :custom
  (treemacs-project-follow-mode t))

(use-package treesit-auto
  ;; Enabling treesit requires GCC.  Disabling on Windows for now.
  :if (not (memq window-system '(w32)))
  :ensure t
  :custom
  (treesit-auto-install 'prompt)
  :config
  (treesit-auto-add-to-auto-mode-alist 'all)
  (global-treesit-auto-mode))

(when (not (memq window-system '(w32)))
  (use-package vterm
    :ensure t
    :defer t
    :init
    (defun vterm-from-current-directory ()
      "Name vterm buffer by the current directory"
      (interactive)
      (if (not (null default-directory))
          (vterm (concat "*vterm*<"
                         (file-name-nondirectory
                          (directory-file-name
                           (file-name-directory default-directory)))
                         ">"))
        (vterm)))
    :custom
    (vterm-max-scrollback 100000)))

(use-package web-mode
  :ensure t
  :defer t
  :custom
  (web-mode-engines-alist '(("php" . "\\.phtml\\'")
                            ("blade" . "\\.blade\\.")))
  :config
  (add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode)))

(use-package with-editor
  :ensure t
  :defer t
  :config
  (global-set-key (kbd "<remap> <async-shell-command>")
                  #'with-editor-async-shell-command)
  (global-set-key (kbd "<remap> <shell-command>")
                  #'with-editor-shell-command)
  :hook (((shell-mode eshell-mode term-exec vterm-mode) .
          with-editor-export-editor)
         ((shell-mode eshell-mode term-exec vterm-mode) .
          with-editor-export-git-editor)))

(use-package xclip
  :ensure t
  :if (not (eq system-type 'windows-nt))
  :config (xclip-mode 1))

(use-package yasnippet
  :ensure t
  :defer t
  :hook (prog-mode . yas-minor-mode)
  :config
  (yas-reload-all)
  (yas-global-mode -1))

(use-package yaml-mode
  :ensure t
  :defer t)

(use-package zenburn-theme
  :ensure t
  :init (load-theme 'zenburn t)
  :config
  (zenburn-with-color-variables
    (custom-theme-set-faces
     'zenburn
     `(highlight ((t (:background ,zenburn-bg-1)))))))

;; (when (and (string-equal (system-name) "debian-hx90")
;;            (file-directory-p "~/.cache/mu"))
;;   (use-package mu4e-debian-hx90))

(provide 'init)
;;; init.el ends here
