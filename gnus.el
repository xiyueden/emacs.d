(require 'ezgnus)

(setq ezgnus-default-user-name "Xiyue Deng"
      ezgnus-default-email "manphiz@gmail.com"
      ezgnus-default-send-server "smtp.gmail.com"
      ezgnus-default-signature "Xiyue Deng")

(ezgnus-news-setting
 :server "news.gmane.io"
 :topic-list '("gmane.emacs.gnus.general"
               "gwene.com.djangoproject.rss.community.blogs"
               "gwene.org.debian.planet"
               "gwene.org.golang.blog"
               "gwene.org.perl.blogs"
               "gwene.org.python.planet"
               "gwene.org.xfce.blog"))

(ezgnus-gmail-setting
 :email "manphiz@gmail.com"
 :authenticator 'xoauth2
 :name "Xiyue Deng"
 :signature "Xiyue Deng")

(ezgnus-gmail-setting
 :email "dengxiyue@gmail.com"
 :authenticator 'xoauth2
 :name "Xiyue Deng"
 :signature "Xiyue Deng")

(ezgnus-gmail-setting
 :email "xiyueden@usc.edu"
 :authenticator 'xoauth2
 :name "Xiyue Deng"
 :signature-file (concat user-emacs-directory
                         "signature.xiyueden.usc"))

(ezgnus-gmail-setting
 :email "xiyuedeng@gmail.com"
 :name "Xiyue Deng")

(ezgnus-gmail-setting
 :email "xiyueden@gmail.com"
 :name "Xiyue Deng")

(ezgnus-gmail-setting
 :email "phurphy@gmail.com"
 :name "Phurphy")

(ezgnus-gmail-setting
 :email "bamphiz@gmail.com"
 :name "Bamphiz")

(ezgnus-gmail-setting
 :email "henrylaioffer@gmail.com")

;;; Outlook.com emails doesn't support password anymore and OAuth2 access token
;;; refresh has been disabled.

;; (ezgnus-outlook-setting
;;  :email "dengxiyue@outlook.com"
;;  :name "Xiyue Deng"
;;  :signature "Xiyue Deng")

;; (ezgnus-outlook-setting
;;  :email "manphiz@outlook.com"
;;  :name "Manphiz")

;; (ezgnus-outlook-setting
;;  :email "xiyuedeng@outlook.com"
;;  :name "Xiyue Deng")

;; (ezgnus-outlook-setting
;;  :email "xiyueden@outlook.com"
;;  :name "Xiyue Deng")

;; (ezgnus-outlook-setting
;;  :email "yeelao@outlook.com")

;; (ezgnus-outlook-setting
;;  :email "yeemanphiz@outlook.com")

;; (ezgnus-outlook-setting
;;  :email "emanphiz@outlook.com")

;; (ezgnus-outlook-setting
;;  :email "leekiki0401@outlook.com")

;; (ezgnus-outlook-setting
;;  :email "youlili0229@outlook.com")

;; (ezgnus-outlook-setting
;;  :email "youyiyouyilin@outlook.com")

;; (ezgnus-outlook-setting
;;  :email "yiyouo4m@outlook.com")

;; (ezgnus-outlook-setting
;;  :email "yiyouo4m@outlook.com")

;; (ezgnus-outlook-setting
;;  :email "ld8487@outlook.com")

;; (ezgnus-outlook-setting
;;  :email "ld848701@outlook.com")

;; (ezgnus-outlook-setting
;;  :email "ld848702@outlook.com")
