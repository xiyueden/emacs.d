;; notmuch-config.el --- Notmuch configuration -*- coding: utf-8; lexical-binding: t -*-

;;; code

;; General
(require 'notmuch)

(setq message-kill-buffer-on-exit t
      mail-specify-envelope-from t
      message-sendmail-envelope-from t
      notmuch-always-prompt-for-sender t
      notmuch-hello-thousands-separator ","
      notmuch-mua-user-agent-function #'notmuch-mua-user-agent-full
      message-signature-file (concat user-emacs-directory "signature.default"))

(setq notmuch-saved-searches
      '(;; Default at the front.  Should be kept in sync on each update.
        (:name "inbox" :query "tag:inbox" :key "i")
        (:name "unread" :query "tag:unread" :key "u")
        (:name "flagged" :query "tag:flagged" :key "f")
        (:name "sent" :query "tag:sent" :key "t")
        (:name "drafts" :query "tag:draft" :key "d")
        (:name "all mail" :query "*" :key "a")

        ;; Custom
        (:name "recent" :query "date:1week..now" :key "r")
        ( :name "debian flagged"
          :query "tag:debian AND tag:flagged AND NOT tag:trashed AND NOT tag:spam"
          :key "b")
        ( :name "emacs flagged"
          :query "tag:emacs AND tag:flagged AND NOT tag:trashed AND NOT tag:spam"
          :key "e")))

;; Set `X-Message-SMTP-Method' according to sender address in the `From' header.
(setq message-server-alist
      '(("manphiz@gmail.com"
         .
         "smtp smtp.gmail.com 587 manphiz@gmail.com")
        ("dengxiyue@gmail.com"
         .
         "smtp smtp.gmail.com 587 dengxiyue@gmail.com")
        ("xiyueden@usc.edu"
         .
         "smtp smtp.gmail.com 587 xiyueden@usc.edu")
        ("xiyueden@gmail.com"
         .
         "smtp smtp.gmail.com 587 xiyueden@gmail.com")
        ("xiyuedeng@gmail.com"
         .
         "smtp smtp.gmail.com 587 xiyuedeng@gmail.com")
        ("bamphiz@gmail.com"
         .
         "smtp smtp.gmail.com 587 bamphiz@gmail.com")
        ("phurphy@gmail.com"
         .
         "smtp smtp.gmail.com 587 phurphy@gmail.com")
        ("henrylaioffer@gmail.com"
         .
         "smtp smtp.gmail.com 587 henrylaioffer@gmail.com")))

;; Disable display-line-numbers-mode in notmuch-hello as it seems to garble the
;; display.
(add-hook 'notmuch-hello-mode-hook #'(lambda () (display-line-numbers-mode 0)))

;; Show more message headers.  Also need to set `extra_headers' in notmuch
;; config.
(setq notmuch-message-headers
      '("From" "To" "Subject" "Cc" "Date" "User-Agent" "X-Mailer"
        "X-Newsreader"))

;; Don't forget attachments (https://notmuchmail.org/emacstips/#index26h2)
(add-hook 'message-send-hook #'notmuch-mua-attachment-check)

;;; notmuch-config.el ends here
